/* recN1.h     11/04/19
     réception au niveau 1 du séquenseur */

#include <tache.h>
#include <recN0.h>

#ifndef REC1_
#define REC1_

#define TPILE (TTABREC+2)

class RecN1 : public TacheGen
{
 Octet tache ();
 RecN0 * ptrRecN0;
 TmpRec tmpRec_0, tmpRec_1, tmpRec_2, tmpRec_3, tmpRec_4, tmpRec_5,
        tmpRec_6, tmpRec_7, tmpRec_8, tmpRec_9;
 Octet xRec;//mis dans dans pileRec, pour mémoriser tmpRec_x
 Octet pileRec[TPILE];//fifo pour traitement séquentiel des xRec->tmpRec_x
 unsigned xPile;//index pour gestion pile (pileRec)
 struct etatRec
  {
  unsigned nExtr;//nombre d'octets extraits par read()
  Octet * ptRec;//pointe un tmpRec_x
  } tabEtat[TTABREC];

 void iniTabRec(void); 

 int xCour ; //indice pour tmpChr
 
 Octet nbCarTmp; //nombre d'octets dans le tampon
 Octet tmpTrt[TTRAIT];

public:
 RecN1( int info1= NOINFO, unsigned etape = DEBUT) : TacheGen(info1, etape)
  {
  iniTabRec();
  xPile =0 ;
  xCour =0;
  }

 Octet *  getPtRec(void);

 void setNbRec(Octet nbRead);

};

#endif //REC1_
