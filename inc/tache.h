/*  tache.h   4/04/19
       täche générique
       */
#include <def_gen.h>

#ifndef TACHE_
#define TACHE_

#define NIV0 0 //tâches de niveau 0
#define NIV1 1
#define NIV2 2
/***********************/
// liste des options du séquenceur (cas 3 niveaux)
#define NBTANIV0 2 // nombre de tâches possibles pour le niveau 0
#define NBTANIV1 2 // nombre de tâches possibles pour le niveau 1
#define NBTANIV2 1 // nombre de tâches possibles pour le niveau 2
/* le nombre de tâche(s) par niveau ne doit jamais être nul (mini = 1)
Par contre  celui-ci peut être supérieur au nombre effectif de tâches, s'il est
inférieur une erreur sera détectée lors du montage */

#define PRIORNIV0 1 //nb d'exécutions dans le niv0 avant de passer au niv1
#define PRIORNIV1 1 //nb d'exécutions dans le niv1 avant de passer au niv2
/* L'idée est de limiter le nombre d'exécutions consécutives par niveau et donc d'être
 < (ou =) au nombre de tâches de ce niveau. Cependant, le fonctionnement reste possible
 même s'il est >. Mais cela risque de pénaliser inutilement les niveaux supérieurs 
 vis à vis du temps réel. A noter que si PRIORNIV1 est surdimentionné la scrutation du
 niveau 0 sera "favorisée" (ie scrutation effectuée après chaque tâche du niveau 1) .
 Les niveaux 0 et 1 peuvent être absents. La sortie du séquenceur se produit lorsque
 plus aucune tâche n'est réactivable; c'est aussi le cas si le niveau 2 est absent. */

/* Pour éviter des compilations inutiles, tenir cette liste à jour !
              fonctionnalités                nombre d'instances 

niveau 0       
               réception N0                       1
               émission  N0                       1
               
Total= 2
----------------------------------------------------------------------
niveau 1       
               réception N1                       1
               émission  N1                       1
Total= 1
----------------------------------------------------------------------
niveau 2      
               gestionnaire                       1
Total= 1
       
*/


/*---------------------*/
#define MAX_ETAPE 1000 //Nb maxi permis d'étapes pour une fonction
#define DEBUT 1 //1ere étape d'une tâche
#define FIN  0 //dernière étape d'une tâche
#define NOINFO -1 //pas d'information à transmettre
#define PASFINI 1 //tâche pas finie, à réactiver (permanente ou non)
#define NEPASREAC 2 //tâche finie, ne pas démonter ni réactiver (permanente)
#define ACTIV 1  // active la tâche dès le montage
#define NONACTIV 0 // le contraire, doit être activée ultérieurement

/**********************/

class TacheGen   //classe abstraite
{
 unsigned etapeInit;//étape initiale: au montage ou pour nouvelle exécution
 Octet numNiv; //mémorise le niveau de la tâche (0, 1, 2, ...)
 Octet rang;//mémorise le rang dans le niveau
protected:
   //doivent être "protected" pour être utilisés par les classes dérivées
 int information1;//est susceptible de contenir l'idT du déclarant, ou autre info. ...
 Octet idT;//identificateur de la tâche= decalNiv + rang
 unsigned memEtape;// mémorise l'étape entre 2 exécutions (FIN pour terminer)
 int getInfo1();
public:
 TacheGen (int info1=NOINFO, unsigned etape= DEBUT); //constructeur
 /*Remarque: les initialisations par défaut peuvent être éventuellement inopérantes si un
   constructeur est défini dans une classe fille . Celui-ci sera alors en charge des initialisations
   par défaut*/
 //~TacheGen(); //destructeur non utilisé


 // une classe abstraite peut être obtenue avec fonction virtuelle pure 
 virtual Octet tache () = 0;

 void caracteriser (Octet passDecalNiv, Octet passRang);

 Octet indicat()//fonction en ligne, retourne l'identificateur de la tâche
 {
 return idT;
 }

 Octet renvNiv() //fonction en ligne, retourne le niveau d'exécution de la tâche
 {
 return numNiv;
 }

 void actitache( unsigned nouvEtape); //activation d'une tâche
 void actitache( ); //surchage : etapeInit inchangé

 Octet finTache() /*fonction en ligne
            La dernière instruction d'une tâche doit être: return finTache(); */
 {
 if ( memEtape ) // memEtape non nul (!= FIN) =>l'exécution n'est pas terminée
   return PASFINI ;
  else //exécution terminée : dans ce cas memEtape = FIN
   {
   memEtape = etapeInit;
   return NEPASREAC;
   }
 }
};

/*-----------------*/
typedef TacheGen * PtrTache; //déclaration globale d'un type pointeur sur les tâches
/*-----------------*/

#endif  // TACHE_

