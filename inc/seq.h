/* seq.h  4/04/19
*/

#include <def_gen.h>
#include <tache.h>

#ifndef SEQ_
#define SEQ_

#define PASFORCER 0 //ne pas forcer le démontage (si tâche activée)
extern const unsigned NBMAXT; //défini dans seq.cpp (en GLOBAL!)
#define NONIMPOS NBMAXT//identification de la tâche n'est pas imposée

/*-----------------*/
class Niveau
{
 Octet nbTaches ; //nombre de tâches : de 1  16
 unsigned actNiv, reactNiv;//variables "activation", "réactivation"
 PtrTache * repertoire;//pour création tableau dynamique de tâches
 Octet encours;//mémorise, pour le prochain passage, où recommencer...
 Octet index;//usages divers
 Octet marq; //autre index...
 Octet repNiv;//en fait décalage pour ce niveau pour s'indexer ds les tables
public:
 Niveau(Octet, Octet); //arguments: nombre de tâches pour ce niveaau,
                    //repérage du niveau: en fait valeur du décalage pour ce niveau

 unsigned monTache(TacheGen * clQq, Octet passActiv= ACTIV, Octet passRang= NONIMPOS );
    //renvoie le rang de la tâche du niveau impliqué

 unsigned progresser();
         //Valeur de retour: 0 si pas de tâche activée, dans
         //tous les cas "variable activation"

 unsigned activer(Octet);
         //argument: identificateur de la tâche à activer
         //Valeur de retour: ce même identif. ou code erreur

 unsigned demonter(Octet passIdt, Octet = PASFORCER );
         //arguments: identificateur de la tâche à démonter,
         // n'agit qu'au niv séquenceur: ne fait pas le "delete" de la tâche.
         //indicateur forçage:si vrai, demonte même tâche en cours
         //Valeur de retour: ce même identif. ou code erreur

 void desactiverTout();
         //désactive toutes le tâches de ce niveau

 PtrTache renvPtr(Octet passIdT);
 //renvoi le ptr d'une tâche, passIdT: identificateur de la tâche
};
/*-------------------------------------------------*/
class Seq3Niv
// pour séquenceur  3 niveaux
{
 unsigned ctrPrior0 ;
 unsigned maxNiv0; //nb d'exécutions max ds le niv0 avant passage Niv1
 unsigned ctrPrior1;
 unsigned maxNiv1;//nb d'exécutions dans le niv1 """" Niv2
public: 
 Niveau niv0, niv1, niv2;
 
 Seq3Niv(Octet nbTniv0, Octet nbTniv1, Octet nbTniv2, unsigned prior0,
                         unsigned prior1 ) : niv0(nbTniv0, 0), 
                         niv1(nbTniv1, nbTniv0), niv2(nbTniv2, nbTniv0+nbTniv1)
 {
 ctrPrior0 =0; ctrPrior1 =0;
 maxNiv0= prior0 ; maxNiv1= prior1 ;
 }

 void sequencer();
};

#endif  // SEQ_
