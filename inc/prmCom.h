/* prmCom.h     paramètres port série COM1 (seulement) ou adaptateur usb <-> RS232
                 4/04/19 */

#include <def_gen.h>
#include <sys/io.h> //pour ioperm()

#ifndef COMS_
#define COMS_

//Définition de la vitesse du port (en bauds)
// #define VITDEF "9600"
#define VITDEF "1200"

int verifVitesse (char * entVit);

int initCom1(int indexVit);
     /*--------------------------*/

#endif //COMS_
