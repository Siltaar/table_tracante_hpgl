/* gestN2.h   6/04/15
     gestionnaire */

#include <tache.h>
#include <emN1.h>

#ifndef GEST_
#define GEST_

using namespace std;//pour iostream et fstream

class GestN2 : public TacheGen
{
 Octet tache ();
 EmN1 * ptrEmN1;
 int idEmN1; //identifiant de la tâche emN1
public:
 GestN2( int info1= NOINFO, unsigned etape = DEBUT) : TacheGen(info1, etape) { }
};
#endif //GEST_
