/* recN0.h 7/04/19
   réception au niveau 0 du séquenceur */

#include <tache.h>
#include <def_gen.h>

#ifndef REC0_
#define REC0_

class RecN0 : public TacheGen
{
 Octet tache ();
public:
 RecN0( int info1= NOINFO, unsigned etape = DEBUT) : TacheGen(info1, etape) { }
};

#endif //REC0_
