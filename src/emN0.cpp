/* emN0.cpp  9/04/19
  émission niveau 0 */

#include <emN0.h>
#include <iostream>
#include <assert.h>
#include <unistd.h>

#define INITEMN0 DEBUT
#define EMN0_1 (INITEMN0+1)

extern int portId;
extern int ctrInit;
extern char tmpEm[];
extern int nbCarEm; //nombre de caractères à émettre
extern int emLib; //émission libre (ou non)

Octet EmN0::tache()
{
int emission;
switch(memEtape)
 {
 case INITEMN0 :
  ctrInit++;
  #ifdef TRACE1
  std::cout << " emN0 OK, idT= "<< (int)indicat() <<"  --info1= "<< getInfo1() <<"\n";
  std::cout<<"ctrInit = "<<ctrInit<<" \n";
  #endif
  memEtape = EMN0_1;
  break;
 case EMN0_1 :
  if ( (nbCarEm)  && (emLib ==VRAI))
    {
    emission = write(portId, tmpEm, nbCarEm);
    if ((emission < nbCarEm) || (emission <0))
      {
      memEtape = FIN;
      emLib = -1;
      std::cout << "erreur émission: demandé = " << nbCarEm<< " --émis = " << emission << "\n";
      }      
     else
      {
      //std::cout << "emN0 nb carEm =  "<< emission << "\n";
      emLib = 1;
      nbCarEm =0;
      memEtape = EMN0_1;
      }
    }
  break;
 default :
  assert(0);
 }
return finTache();
}

