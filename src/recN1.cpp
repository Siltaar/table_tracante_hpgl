/* recN1.cpp     7/04/19   %27/04/19
      réception au niveau 1 */

#include <recN1.h>
#include <seq.h>
#include <recN0.h>
#include <syst1.h>
#include <assert.h>
#include <iostream>
#include <unistd.h>

#define INITRECN1 DEBUT
#define RECN1_1 (INITRECN1 +1)
#define RECBCL (RECN1_1 +1)
#define AFFICHE (RECBCL +1)
#define TRAIT (AFFICHE +1)

extern Seq3Niv sequenceur;
extern int ctrInit;
extern RecN0 * ptrRecN0;
extern unsigned ctrPasN2;

Octet RecN1 :: tache()
{
unsigned div ;
Octet xTmp ;
Octet * ptrTmpCours ;
int j ;
switch(memEtape)
 {
 case INITRECN1 :
  ctrInit++;
  #ifdef TRACE1
  std::cout << "recN1 : OK, idT= "<< (int)indicat()<<"  --info1= "<< information1 <<"\n";
  std::cout<<"ctrInit = "<<ctrInit<<" \n";
  #endif
  ptrRecN0 = new RecN0 ();
  assert(ptrRecN0 != NULL);
  div = sequenceur.niv0.monTache(ptrRecN0);
  if (div >= NBMAXT)
    affErr(div, NIV0);
  memEtape = RECN1_1;
  break;
 case RECBCL :
   memEtape = RECBCL; //on boucle pour ne pas perturber l'émission
  break; 
 case AFFICHE :
  std::cout << "\n";
  memEtape = RECN1_1;
  break;
 case TRAIT :
  j=0;
  while (j < nbCarTmp )
   {
   if ( (tmpTrt[j] >= 32) && (tmpTrt[j] <= 126))
     {
     std::cout << " "<< tmpTrt[j];
     }
    else
     std::cout<<" 0x:"<<std::hex <<(int) tmpTrt[j];
   j++;
   }
  memEtape = AFFICHE;
  break;

 case RECN1_1 :
  memEtape = RECN1_1;
  if (xPile) //au moins un tmpRec_x à traiter
    {
    int j ;
    ctrPasN2 =0;
    xTmp = pileRec[0];//pour tampon le plus ancien
    nbCarTmp = tabEtat[xTmp].nExtr;
    ptrTmpCours = tabEtat[xTmp].ptRec;
    xCour =0;
    j=0;
    while (j < nbCarTmp )
     {
     tmpTrt[xCour++] = *ptrTmpCours++;
     j++;
     if (xCour >= TTRAIT)
       break;
     }
    if (xCour >= TTRAIT)
      {
      std::cout << "débordement de tampon dans réception -> HS\n";
      memEtape = RECBCL;
      }
     else
      {
      memEtape = TRAIT;
      }
    tabEtat[xTmp].nExtr =0; //libérer le tampon
    xPile--; //mettre la pile à jour
    for( div= 0; div < xPile; div++)
      pileRec[div] = pileRec[div +1];
    }
  break;
 default :
  assert(0);
 }
return finTache();
}

Octet * RecN1::getPtRec(void)
 {
 Octet * ptr;   
 for (xRec=0; xRec<TTABREC; xRec++)
  if ( tabEtat[xRec].nExtr ==0)
    {
    ptr = tabEtat[xRec].ptRec;
    break;
    } 
 if (xRec == TTABREC)
   ptr = NULL;
 return ptr;
 }

void RecN1::setNbRec(Octet nbRead)
 {
 pileRec[xPile++] = xRec;
 if (xPile >= TPILE)
   {
   std::cout << "ATTENTION: pile réception saturée \n";
   }
 tabEtat[xRec].nExtr = nbRead;
 }

void RecN1::iniTabRec(void)
 {
 tabEtat[0].nExtr =0; tabEtat[0].ptRec = tmpRec_0;
 tabEtat[1].nExtr =0; tabEtat[1].ptRec = tmpRec_1;
 tabEtat[2].nExtr =0; tabEtat[2].ptRec = tmpRec_2;
 tabEtat[3].nExtr =0; tabEtat[3].ptRec = tmpRec_3;
 tabEtat[4].nExtr =0; tabEtat[4].ptRec = tmpRec_4;
 tabEtat[5].nExtr =0; tabEtat[5].ptRec = tmpRec_5;
 tabEtat[6].nExtr =0; tabEtat[6].ptRec = tmpRec_6;
 tabEtat[7].nExtr =0; tabEtat[7].ptRec = tmpRec_7;
 tabEtat[8].nExtr =0; tabEtat[8].ptRec = tmpRec_8;
 tabEtat[9].nExtr =0; tabEtat[9].ptRec = tmpRec_9;
 }

