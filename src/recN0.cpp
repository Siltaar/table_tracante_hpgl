/* recN0.cpp   11/04/19
   réception  au niveau 0 du séquenceur */

#include <recN0.h>
#include <recN1.h>
#include <assert.h>
#include <iostream>
#include <unistd.h>

#define INITREC0 DEBUT
#define REC0_1 (INITREC0+1)

extern int portId;
extern int ctrInit;
extern RecN1 * ptrRecN1;
extern unsigned ctrPasN2;

Octet RecN0::tache()
{
Octet * pt;
Octet nbLus;
switch(memEtape)
 {
 case INITREC0 :
  ctrInit++;
  #ifdef TRACE1
  std::cout << "recN0 : oK, idT= "<< (int)indicat() <<"  --info1= "<< getInfo1() <<"\n";
  std::cout<<"ctrInit = "<<ctrInit<<" \n";
  #endif
  assert(ptrRecN1 != NULL);
  memEtape = REC0_1;
  break;

 case REC0_1 :
  memEtape = REC0_1;
  pt = ptrRecN1->getPtRec();
  if ( pt != NULL)
    {
    if ( (nbLus = read(portId, pt, (TTMPREC))) != 0) //caractères reçus
      {
      ptrRecN1->setNbRec(nbLus);//provoque la "prise" du tampon
      #ifdef TRACE3
      std::cout<<"\n nbLus: "<<int(nbLus)<<" \n";
      #endif
      ctrPasN2=0;
      }
    }
   #ifdef TRACE3
   else
    std::cout<<"renvoi d'un pointeur nul: la réception est saturée !!\n";
   #endif
  break;
 default :
  assert(0);
 }
return finTache();
}
