/* seq.cpp   séquenseur    4/04/19  */

#include <seq.h>
#include <assert.h>
#include <syst1.h>

#define NULL 0

/* définition de masques pour sélection d'un bit
                                         sur un entier (short = 16 bits) */
enum {msq0=1,msq1=2,msq2=4,msq3=8,msq4=0x10,msq5=0x20,msq6=0x40,msq7=0x80
                             ,msq8=0x100,msq9=0x200,msq10=0x400,msq11=0x800
                     ,msq12=0x1000,msq13=0x2000,msq14=0x4000,msq15=0x8000 };

// GLOBAL !!
const unsigned NBMAXT = 8 * sizeof (Mot);//nombre maximal de tâches par niveau
        // 8 bits par octet, 1 Mot -> 2 octets => NBMAXT =16  
        //Mot est déclaré unsigned short
const Mot TabMsq[ NBMAXT ]={msq0,msq1,msq2,msq3,
          msq4,msq5,msq6,msq7,msq8,msq9,msq10,msq11,msq12,msq13,msq14,msq15};
/*-----------------------------------------------------*/
// GLOBAL !!
Seq3Niv sequenceur(NBTANIV0, NBTANIV1, NBTANIV2, PRIORNIV0, PRIORNIV1);
   //objet de classe stockage "statique"
/*-------------------------------------------------*/
TacheGen::TacheGen( int info1,  unsigned etape)
{
etapeInit = etape;
memEtape = etape;
information1 = info1;
}

/*
TacheGen::~TacheGen()
{
} */

int TacheGen::getInfo1()
{
return information1;
}

void TacheGen::caracteriser(Octet passDecalNiv, Octet passRang)
{
idT = passDecalNiv + passRang;//pour s'indexer ds les tables
rang = passRang;
numNiv = NIV2; //sauf avis contraire ds la suite...
if ( passDecalNiv < NBTANIV0 + NBTANIV1)
  numNiv = NIV1; //écrase le précédent
if ( passDecalNiv < NBTANIV0)
  numNiv = NIV0; //écrase ...
}

void TacheGen::actitache()
{ //surcharge de actitache(unsigned) : pas de changement de la 1e étape
unsigned provi=ERREUR ;//usage divers
switch (numNiv)
 {
 case NIV0 :  //activation au niveau 0
  provi=sequenceur.niv0.activer(rang);
  if ( provi != rang)
    affErr(provi, NIV0);
  break;
 case NIV1 :  //activation au niveau 1
  provi=sequenceur.niv1.activer(rang);
  if ( provi != rang)
    affErr(provi, NIV1);
  break;
 case NIV2 :  //activation au niveau 2
   provi=sequenceur.niv2.activer(rang);
   if ( provi != rang)
     affErr(provi, NIV2 );
   break;
 default :
  assert(0);
 }
}

//---------------------------------

Niveau::Niveau(Octet passNbT, Octet passDecal)
{
 assert ( passNbT <= NBMAXT );
 nbTaches = passNbT;
 repNiv = passDecal; //repérage niv = décalage pour ce niveau
 actNiv=0 ; reactNiv=0 ; //rien d'activé à la création
 encours = 0;
 repertoire = new PtrTache[ nbTaches];
 assert ( repertoire != NULL );
 for (Octet index=0; index < nbTaches; index++) repertoire[index]= NULL;
}
//---------------------------------
unsigned Niveau::monTache(PtrTache clasQq, Octet passActiv, Octet passRang)
{
unsigned compteRendu;
if ( passRang >= nbTaches ) //le rang de la tâche n'est pas imposé
  {                          // remarquer que NONIMPOS  >= nbTaches
  compteRendu = NIVEAUPLEIN ; // si pas de place trouvée
  for (index=0; index < nbTaches; index++)
   {     //rechercher une place libre
   if ( repertoire[index] ==  NULL )
     {
     compteRendu = index; //renvoie le rang de la tâche ds ce niveau
     repertoire[index] =  clasQq;
     (repertoire[index])->caracteriser( repNiv, index);
     index = nbTaches; //pour sortir de la boucle
     }
   }
  }
 else //le rang est imposé, nombre de 0 à nbTaches (< 16)
  {
  if (repertoire[passRang] ==  NULL)
    {
    compteRendu = passRang; //renvoie le rang de la tâche
    repertoire[passRang] = clasQq;
    (repertoire[index])->caracteriser( repNiv, index);
    }
   else
    compteRendu = DEJAUTILISE;
  }
if (passActiv && (compteRendu < nbTaches) ) // pas d'erreur=tâche montée
  actNiv |= TabMsq[ compteRendu ];// la tâche est activée lors du montage
  //si non la tâche doit être activée ultétieurement
return compteRendu;
}
//---------------------------------
unsigned Niveau::progresser()
{
if ( actNiv)//au moins un tâche activée, alors rechercher la 1ère
  {
  for (marq=encours; marq < nbTaches; marq++)
   { //recherche de la 1ere tâche activée
   if ( TabMsq[marq] & actNiv ) //le tâche est activée
     {
     actNiv &= ~TabMsq[marq];//RAZ de l'activation
     Octet faire = repertoire[marq]->tache() ;
     switch ( faire )
      {
      case PASFINI :
       reactNiv |= TabMsq[marq] ;
       break;
      case NEPASREAC :
       break;//rien à faire: permanente et finie
      default :
       assert(0) ;
      }
     marq = nbTaches; //pour sortir et redonner la main au niv n-1
     }
     encours ++; //pour le prochain passage
     if (encours >= nbTaches) //niveau fini pour "ce tour"
       {
       encours =0;  //pour le prochain passage ds ce niveau
       actNiv |= reactNiv ;
       reactNiv =0;
       }
   }
  }
 else //aucune tâche activée
  {
  encours=0;  //recommence au début pour le prochain passage
  actNiv |= reactNiv ;
  reactNiv =0;
  }
/*retourne 0 si aucune tâche encore activée ( ><0 sinon )*/
return (actNiv|reactNiv);
}
//---------------------------------
unsigned Niveau::activer(Octet passRang)
{ //RMQ l'étape initiale est déterminée à la création (avant montage)
if ( passRang >= nbTaches )
  return IDENTICOMP;
unsigned compteRendu = NONMONTE;
if ( repertoire[passRang] ) //si pointeur non null: tâche existe 
  {
  if ( TabMsq[passRang] & actNiv )//déjà activée
    compteRendu = DEJACTIV;
   else //tâche activable
    {
    compteRendu = passRang;
    actNiv |= TabMsq[passRang];
    }
  }
return compteRendu;
}
//---------------------------------
unsigned Niveau::demonter(Octet passIdt , Octet forcer)
{
Octet rangNiv =passIdt - repNiv;
if ( rangNiv >= nbTaches )
  return IDENTICOMP ;
if ( repertoire[rangNiv] == NULL )
  return NONMONTE; //non montée, ne peut être démontée
if ( forcer)
  {
  actNiv &= ~TabMsq[rangNiv];//RAZ de l'activation
  reactNiv  &= ~TabMsq[rangNiv];//RAZ de la réactivation
  repertoire[rangNiv] = NULL;
  return passIdt;
  }
 else //démontage seulement si exécution non en cours (non forcé)
  {
  if ( (TabMsq[rangNiv] & actNiv) || (TabMsq[rangNiv] & reactNiv) )
    return EXECACTIV ; //pas de démontage :tâche en cours
   else //non en cours d'exécution: démontage possible
    {
    repertoire[rangNiv] = NULL;
    return passIdt;
    }
  }
}
//-----------------------------------------
void Niveau::desactiverTout()
{
actNiv = 0;//RAZ de l'activation
reactNiv  = 0; //RAZ de la réactivation
encours=0;
}
//------------------------------------------------------------
void Seq3Niv::sequencer()
{
while (niv2.progresser()) //au moins une tâche active dans +faible prio
 {
 while ( ctrPrior1 < maxNiv1 )
  {
  while ( ctrPrior0 < maxNiv0 )
   {
   if ( niv0.progresser() ) //au moins une tâche activée /réactivée
     ctrPrior0++;
    else //sortir si plus de tâche à exécuter
     ctrPrior0 = maxNiv0;
   }
   ctrPrior0 = 0; //permet l'exécution du niveau n-1
   if (niv1.progresser() ) //au moins une tâche active
     ctrPrior1++;
    else
     ctrPrior1 = maxNiv1;
  }
 ctrPrior1 = 0; //permet l'exécution du niveau n-1
 }
}
//---------------------------------
PtrTache Niveau::renvPtr(Octet passIdT)
{
passIdT -= repNiv;//obtient le rang ds le niveau à partir de l'identificateur
return repertoire[passIdT];
}

