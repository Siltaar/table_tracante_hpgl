/* syst1.cpp   4/04/19
*/
#include <syst1.h>
#include <iostream>

const char * msgErr[] = {
/*ERREUR 1000*/ "erreur generique",
/*IDENTICOMP 999 */ "identif demandé incompatible (trop grande ...)",
/*NONMONTE   998 */ "indentificateur ne correspond à aucune tâche montée",
/*DEJACTIV   997 */ "ne peut être activée car déjà activée",
/*NIVEAUPLEIN 996 */ "plus de place pour montage dans le niveau : ",
/*DEJACT      995 */ "tache deja activee",
/*DEJAUTILISE 994 */ "identif est déjà prise par une autre fonction",
/*EXECACTIV   993 */ "en cours d'exécution, ne peut être démontée",

/*PLUSPTE     992 */  "erreur ds erreur !!"
};

void affErr(unsigned noErr, int entCompl)
{
if (noErr > PLUSPTE)
  {
  if (entCompl == SANS) //cas sans info complémentaire
    std::cout << msgErr[ERREUR-noErr]<<"\n";
   else
    std::cout << msgErr[ERREUR-noErr]<<"  "<< entCompl <<"\n";
  }
 else //cas limite du traitement erreur
  std::cout << msgErr[ERREUR-PLUSPTE]<<"\n";
}
