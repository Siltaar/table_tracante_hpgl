/* gestN2.cpp    6/04/19  %11/04/19
     gestionnaire pour table traçante */

#include <seq.h>
#include <gestN2.h>
#include <syst1.h>
#include <assert.h>
#include <iostream>
#include <unistd.h>
#include <string.h>

extern Seq3Niv sequenceur;
extern char tmpUtil[];
extern int tabSig[];
extern int ctrInit;
extern int nbCarEm; //nombre de caractères à émettre
extern char tmpEm[];
extern int flagRec;
extern unsigned ctrPasN2;
extern int lNom;

#define INITGEST DEBUT //phase d'initialisation
#define ATTNOM (INITGEST +1) //attente de l'entrée d'un de fichier (ou q)
#define ATFINEM (ATTNOM +1) //attente fin émission
#define ATTEM (ATFINEM +1) //attente émission libre
#define ATTINIT (ATTEM +1) //attente fin initialisation
#define ATREC (ATTINIT +1)

Octet GestN2 :: tache()
{
unsigned div;
switch(memEtape)
 {
 case INITGEST :
  ctrInit++;
  #ifdef TRACE1
  cout << " gestN2 OK, idT= "<< (int) idT <<"  --info1= "<<getInfo1() <<"\n";
  std::cout<<"ctrInit = "<<ctrInit<<" \n";
  #endif
  ptrEmN1 = new EmN1((int)indicat()); //passe son id à EmN1 
  assert(ptrEmN1 != NULL);
  div = sequenceur.niv1.monTache(ptrEmN1);
  if (div >= NBMAXT)
    affErr(div, NIV1);
  idEmN1= (int)ptrEmN1->indicat() ;
  memEtape = ATTINIT; 
  break;
 case ATTINIT :
  if (ctrInit >= NBINIT)
    { 
    memEtape = ATTNOM;
    }
   else
    memEtape = ATTINIT;
  break;
 case ATTNOM :
  #ifdef TRACE1
  cout<<"long= "<<lNom<<"  buf= "<<tmpUtil <<"\n";
  #endif
  if (lNom ==2)
    {
    if (tmpUtil[0] == 'q')
      memEtape =FIN;
     else
      {
      if (tmpUtil[0] == '1')
        {
        std::cout<<"commande 1 "<<"\n";
        tmpEm[0] = 27; //ESC
        tmpEm[1] = '.';
        tmpEm[2] = 'B';
        tmpEm[3] = ';';
        tmpEm[4] = 10 ; //LF
        nbCarEm = 5;
        ctrPasN2 =0;
        memEtape = ATREC;
        }
      }
    } 
   else
    {
    tmpUtil[lNom]= '\0';
    memEtape = ATTEM;
    }
  break;
 case ATTEM :
  if ((tabSig[idEmN1] == NOSIG) && (!nbCarEm))//émission libre
    {
    tabSig[idEmN1] = DEMIS;
    memEtape = ATFINEM ;
    }
   else
    memEtape = ATTEM;
  break; 
 case ATFINEM :
  if ((tabSig[idEmN1] == NOSIG) && (!nbCarEm))
    {
    ctrPasN2 =0;
    memEtape = ATREC;
    }
   else
    memEtape = ATFINEM ;
  if (tabSig[idEmN1] == ABORT) //fin sur émission saturée
    {
    memEtape = FIN;
    }
  
  break;
 case ATREC :
  ctrPasN2++;
  if (ctrPasN2 >= PASN2)
    memEtape = ATTNOM;
   else
    memEtape = ATREC;
  break;
 default :
  assert(0);
 }
return finTache();
}


