/* main.cpp   6/04/19  %11/04/19  %4/05/19
 */

#include <seq.h>
#include <prmCom.h>
#include <tache.h>
#include <syst1.h>
#include <assert.h>
#include <recN1.h> 
#include <gestN2.h> 
#include <emN1.h>
#include <iostream>
#include <stdlib.h> //pour exit()
#include <unistd.h> //pour getopt(), optopt, ...

using namespace std;//pour iostream et fstream, évite std::cout etc...

extern const unsigned TABSIG = NBTANIV0 + NBTANIV1 + NBTANIV2 ; // $$$  

/* variables définies en globales, fichier varGlob.cpp   */
extern char tmpUtil[];
extern int maxFen;
extern int tempo;
extern Seq3Niv sequenceur;
extern const unsigned NBMAXT;
extern int tabSig[];
extern RecN1 * ptrRecN1;
extern int lNom; //longueur de la saisie +1 (le '\0' final)

int main(int argc, char* argv[])
{
unsigned uDiv ; //usages divers
int vit , iDiv; //pour vitesse RS232 et divers...
opterr=0;
char  vitDef[] = {VITDEF};
vit = verifVitesse(vitDef); 

#ifdef USB
cout<<"  --- version usb -> port série (câble nécessaire) ---\n";
#else
cout<<"  --- version port série direct ---\n"; 
#endif

cout<<"Taille d'un bloc de caractères:  "<<TBLOC<<"  paramère= TBLOC \n";
if ( argc < 2)
  {
  cout<<"il faut un nom de fichier\n";
  exit(1);
  }
 else
  {
  iDiv=0;
  while (argv[1][iDiv] !='\0')
   {
   tmpUtil[iDiv]= argv[1][iDiv];
   iDiv++;
   }
  tmpUtil[iDiv]= '\0';
  lNom = iDiv;
  cout<<"merci pour: "<<tmpUtil<<"\n";
  }
cout << "la vitesse est, par défaut: "<< VITDEF<<" bauds : option -v\n";
cout << "la fenêtre est de: "<<maxFen<<" blocs : option -f\n";
cout << "la tempo./fenêtre est de: "<<tempo<<" ms: option -t\n";
if (initCom1(vit) <0 )
  {
  cout << "erreur d'initialisation port RS232\n";
  exit (1);
  }
for (uDiv=0 ; uDiv < TABSIG ; uDiv++) //initialisation de la table des signaux
 tabSig [uDiv] = NOSIG; //pas de sigaux entre tâches
GestN2 * ptrGest = new GestN2 ();//une tâche niveau 2 est indispensable pour le démarrage
assert(ptrGest != NULL);
uDiv = sequenceur.niv2.monTache(ptrGest);
if (uDiv >= NBMAXT)
  affErr(uDiv, NIV2);
 else
  {
  iDiv = (int)ptrGest->indicat() ;
  ptrRecN1 = new RecN1(iDiv);
  assert(ptrRecN1 != NULL);
  uDiv = sequenceur.niv1.monTache(ptrRecN1);
  if (uDiv >= NBMAXT)
    affErr(uDiv, NIV1);
  sequenceur.sequencer();//lance l'exécution des tâches
  }
//----------------
cout << "fin programme \n";
exit(0);
}

