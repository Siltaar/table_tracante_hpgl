/* varGlob.cpp 11/04/19
  déclare les variables globales
*/
#include <def_gen.h>
#include <tache.h>
#include <recN1.h>
#include <recN0.h>

int portId; // identification du port série
const unsigned TABSIG = NBTANIV0 + NBTANIV1 + NBTANIV2 ;
int tabSig[TABSIG]; //table de signaux entre tâches
char tmpUtil[NCNOM +1] ; //tampon pour saisie nom fichier par l'utilisateur "nomSaisie\0" 
int ctrInit =0; //compte les tâches initialisées
int nbCarEm; //nombre de caractères à émettre par emN0 
#ifdef USB
 int maxFen = 1; //nombre de bloc dans une fenêtre (valeur par défaut).
 int tempo = 5500; //temoprisation en ms entre fenêtre
#else
 int maxFen = 3; //nombre de bloc dans une fenêtre (valeur par défaut).
 int tempo = 1570; //temoprisation en ms 
#endif
char tmpEm[TTMPEM]; //tampon émission (entre emN1 emN0)
RecN1 * ptrRecN1;//pour accès aux fonctions de RecN1
RecN0 * ptrRecN0;
unsigned ctrPasN2; //incrémenté au N2 pour attente réception; raz par recN0 et recN1
int emLib = 1; //1 émission libre, 0 non libre sans erreur, -1 erreur
int lNom; //longueur de la saisie +1 (le '\0' final)
